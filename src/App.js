import AppNavbar from './components/AppNavbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Courses from './pages/Courses'
import CourseView from './pages/CourseView';
import Homepage from './pages/Homepage';
import { Container } from 'react-bootstrap';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import  { useEffect, useState } from 'react';
import { UserProvider } from './UserContext';

function App() {

  //Global state hook for the user info for validating if user s logged in 
  const [user, setUser] = useState({
    id:null,
    idAdmin: null
  });

  // function for clearing localstorage on logout 
  const unsetUser = () => {
     localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (

    // We store info in the context by providing the info using the "userProvider" componment and passing the information via the "value" prop
    //All the info inside the value prop will be acessible to pages/components wrapped arounff the UserProvider
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        {/* <Homepage/> */}
        <Container> 
          <Routes>
              <Route path='/' element={<Homepage/>}/>
              <Route path='/courses' element={<Courses/>}/>
              <Route path='/courses/:courseId' element={<CourseView/>}/>
              <Route path='/login' element={<Login/>}/>
              <Route path='/register' element={<Register/>}/>
              <Route path='/logout' element={<Logout/>}/>
              <Route path='*' element={<Error/>}/>
          </Routes>
        </Container> 
      </Router>
    </UserProvider>
  );
}

export default App;
