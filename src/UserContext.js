//we will use react's context API to fivce the logged in user hte "gloabal" scole within our application

import React from 'react';

// Create Contet
// A context with object data type that can be used to stoee info that can be shatred to other components with the app
const UserContext = React.createContext()

// The "Provider" componeent allows other conmponents to consume/use the context ovbject and sipply the necessary info needed to the context object
export const UserProvider = UserContext.Provider

export default UserContext 