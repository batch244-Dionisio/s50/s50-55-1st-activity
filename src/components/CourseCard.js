// import { useEffect, useState } from 'react'; 
import { Card, Button, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

    //deconstruct the course properties into their own  variables
    
    const {name, _id, price, description} = courseProp

    // syntax 
    // const [getter, setter] = useState(uinitialGetterValue)
    // const [count, setCount] = useState(0);

    // // UseState hook for getting and setting the seats for the course
    // const [ seats, setSeats] = useState(30)

    // const [ isOpen, setIsOpen ] = useState(false);
    // function enroll(){
	// 	// if (seats > 0) {
	// 		setCount(count + 1);
	// 		console.log('Enrollees: ' + count);
	// 		setSeats(seats - 1);
	// 		console.log('Seats: ' + seats);
	// 	// } else {
	// 	// 	alert('No more seats available');
	// 	// }
		
	// }

    // Function that keeps track of the enrollees for a course
    // function enroll(){
    //     // if (seats === 0) {
    //         alert ("ay sad di kana naka enroll better luck next year!");
    //         return;
    //     // } else {
    //         setCount(count + 1);
    //         setSeats(seats - 1);
    //     }
    //     console.log('Enrollees:' + count)
    //     console.log('Availble Slots:' + seats)
    // // }

    // Define a "useEffect" hook to have the "CourseCard" component do perform a certain task after every DOM update
            // This is run automatically both after initial render and for every DOM update
    	    // React will re-run this effect ONLY if any of the values contained in the seats array has changed from the last render / update

    // useEffect(() => {
    //     if(seats === 0){
    //         setIsOpen(true);
    //     }
    // }, [seats])

    return (
        <Col className="mt-3 mb-3">
            <Card>
            <Card.Body className='p-3'>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP{price}</Card.Text>
                <Button as={Link} variant="warning" to={`/courses/${_id}`}>Details</Button>
            </Card.Body>
            </Card>
        </Col>
    )
}