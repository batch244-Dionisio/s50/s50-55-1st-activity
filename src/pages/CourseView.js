import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CourseView() {

	// The "useParams" hook allows us to retrieve the courseId passed via the URL
	const { courseId } = useParams();
	
	// ti bale to obtain
	const { user } = useContext(UserContext)

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);

	useEffect(() => {
		console.log(courseId);

		// Fetch request that will retrieve the details of the course from the database to be displayed in the "CourseView" page
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [courseId]);

	// function to enroll a user
	const enroll = (courseId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data == true) {
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: "success",
					text: "You have successfully to this course"
				})
			} else {
				Swal.fire({
					title: "You died!",
					icon: "error",
					text: "You have been slain!"
				})
			}
		})
	}
	
	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					        <Card.Subtitle>Class Schedule</Card.Subtitle>
					        <Card.Text>8 am - 5 pm</Card.Text>
					       
						   {user.id !== null 
						   		?
								   <Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
								:
								<Button as={Link} to="/login"  variant="danger">Log in to Enroll</Button>
						   }

					    </Card.Body>
					</Card>
				</Col>
			</Row> 
		</Container>
	)
}
