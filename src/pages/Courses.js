// import coursesData from "../data/coursesData";
import { useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";

export default function Courses() {

// state that will be used to store the courses retrived from the database\
const [ courses, setCourses ] = useState([]);


// retrieves the courses from the database upon inistual render of the courses component
useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses`)
    .then(res => res.json())
    .then(data => {
        console.log(data);

        // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
        setCourses(data.map(course => {
            return (
                <CourseCard key={course._id} courseProp = {course}/>
            )
        }))
    })
}, [])


    // map method loops through the individual course objects in our array and returns a courseCard component for each course

    // Multiple components created through the map method must have a unique key that will help react JS identify which components/elements have been changed, added, remove
    // everytime the map method loops through the data, i creates a "courseCard" component and then passes the current element in our Courses using the courseProp

    // const courses = coursesData.map(course => {
    //     return (
    //     <CourseCard key={course.id} courseProp = {course}/>
    //     )
    // })

    return (
        <>
       <h1>Courses</h1>
       {courses}
       </>
    )
}