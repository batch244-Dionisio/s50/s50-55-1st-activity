// import video from '../video/error1.mp4'
import Banner from '../components/Banner'

export default function Error() {

    const data = {
        
        title: " 404 - Page Not Found!",
        content: "The page you are looking for cannot not found",
        destination: "/",
        label: "Back home"       
    }

    return (
        <Banner data={data}/>
    )    
} 