import { useEffect, useState, useContext } from 'react'
import {Form, Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Login(){

    // Allows us to consume the user contct object and its to use for user validation
    const {user, setUser} = useContext(UserContext)

    const [email, setEmail] = useState ('');
    const [password, setPassword] = useState ('');
    const [isActive, setIsActive] = useState (false);
    
    console.log(email);
    console.log(password);
    
    function  registerUser(e) {

        e.preventDefault();


        // process a fetch request form the corresponding backend API
        // the header information "content=type" is used to specify that the info being sent tot the backend will be sent in the form JSON 
         // The fetch request will communicate with our BE application providing it with a stringified JSON
        // Convert the info into JS object using .then => (res.json())
        // syntax:
            //  fetch('url, {options})
            //  .then(res => res.json())
            //  .then(data => {})

        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res =>res.json())
        .then(data => {
            console.log(data)

            if(data.access !== undefined) {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "Success",
                    text: "Welcome to Zuitt"
                });
            } else {
                Swal.fire({
                    title: "Failed",
                    icon: "Error",
                    text: "Please Try Again!"
                })
            }
        })

        //user in the localStorage.setItem('propertyname', value)
        // syntax: local
        // localStorage.setItem('email', email)

        // Set the global user state to have properties obtained from local storage
        // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
        // When states change components are rerendered and the AppNavbar component will be updated based on the user credentials, unlike when using the localStorage where the localStorage does not trigger component rerendering

        // setUser({email: localStorage.getItem('email')})

        setEmail('');
        setPassword('')
        
        // alert('You are log in Now!')
    }

    const retrieveUserDetails = (token) => {
        
        fetch('http://localhost:4000/users/details', {
            headers: {
                authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            // changes the glocal user state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if((email !== '' && password !== '')) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password ]);

    
    return ( 

        // conditional rendering that will redirect the iser to the courses page when a user is logged in
       (user.id !== null)
        ?
        <Navigate to ='/courses'></Navigate>
        :
            <Form onSubmit={(e) => registerUser(e)}>
            <h1 className='pt-2 pb-3'>Log in</h1>
            <Form.Group controlId = "userEmail">
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                    type="email"
                    placeholder = "Enter email"
                    required
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
            </Form.Group>

            <Form.Group className='pb-3' controlId = "password">
                <Form.Label> Password</Form.Label>
                <Form.Control
                    type="password"
                    placeholder = "Password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </Form.Group> 

           
            {isActive
                ? 
                    <Button variant="success" type="submit" id="submitBtn">Submit</Button>
                :
                    <Button variant="warning" type="submit" id="submitBtn" disabled>Submit</Button>
            }
            

        </Form>

        )

}