import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, useEffect } from "react";

export default function Logout(){

   const { unsetUser, setUser } = useContext(UserContext);

    // localStorage.clear is a method to clear the info in  our localStorage insuring that no info stored in our browser
     unsetUser();
     
     useEffect(() => {
      setUser({id: null})
     })

     return (
        <Navigate to ='/login'/>
     )
} 